#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

__author__ = "Alin Marin Elena <alin@elena.space>"
__copyright__ = "Copyright© 2017 Alin M Elena"
__license__ = "MIT"
__version__ = "1.0"

import numpy as np
from math import *

def f2(r):
    return d[0]+d[1]*r+d[2]*r**2+d[3]*r**3+d[4]*r**4+d[5]*r**5

def df2(r):
    return d[1]+2*d[2]*r+3*d[3]*r**2+4*d[4]*r**3+5*d[5]*r**4

def f3(r):
    return b[0]+b[1]*r+b[2]*r**2+b[3]*r**3

def df3(r):
    return b[1]+2*b[2]*r+3*b[3]*r**2

def U(r):
    if r<=rc1:
      return a*exp(-r/ρ)
    elif(r<=rm):
      return np.sum([d[i]*r**i for i in range(0,6)])
    elif(r<=rc2):
      return np.sum([b[i]*r**i for i in range(0,4)])
    else:
      return -c/r**6

#G(r)=-r*∂U/∂r
def G(r):
    if r<=rc1:
      return r*a/ρ*exp(-r/ρ)
    elif(r<=rm):
      return np.sum([-i*d[i]*r**(i) for i in range(1,6)])
    elif(r<=rc2):
      return np.sum([-i*b[i]*r**(i) for i in range(1,4)])
    else:
      return -6*c/r**6

#elrc=∫_rcut^∞U(r)r^2dr
def Elrc(rc):
    return -c/(3.0*rc**3)

#vlrc=∫_rcut^∞ r*∂U/∂r*r^2dr
def Vlrc(rc):
    return 2.0*c/rc**3

a=11272.6
ρ=0.1363
c=134.0
rmin=0.0
rc1=1.2
rm=2.1
rc2=2.6

#ngrid=1504
ngrid=1504
rcut=15.0
delpot=rcut/(ngrid-4)

elrc=Elrc(rcut)*9648.530821
vlrc=Vlrc(rcut)*9648.530821

H=np.array([
    [1,rc1,rc1**2,rc1**3  ,rc1**4   ,rc1**5   ,0          ,0  ,0     ,0],
    [0,0  ,0     ,0       ,0        ,0        ,1          ,rc2,rc2**2,rc2**3],
    [0,1  ,2*rc1 ,3*rc1**2,4*rc1**3 ,5*rc1**4 ,0          ,0  ,0     ,0],
    [0,0  ,0     ,0       ,0        ,0        ,0          ,1  ,2*rc2 ,3*rc2**2],
    [0,0  ,2     ,6*rc1   ,12*rc1**2,20*rc1**3,0          ,0  ,0     ,0],
    [0,0  ,0     ,0       ,0        ,0        ,0          ,0  ,2     ,6*rc2],
    [1,rm ,rm**2 ,rm**3   ,rm**4    ,rm**5    ,-1         ,-rm,-rm**2,-rm**3],
    [0,1  ,2*rm  ,3*rm**2 ,4*rm**3  ,5*rm**4  ,0          ,0  ,0     ,0],
    [0,0  ,0     ,0       ,0        ,0        ,0          ,1  ,2*rm  ,3*rm**2],
    [0,0  ,2     ,6*rm    ,12*rm**2 ,20*rm**3 ,0          ,0  ,-2    ,-6*rm]])
F=np.array([a*exp(-rc1/ρ),
           -c/rc2**6,
           -a/ρ*exp(-rc1/ρ),
            6.0*c/rc2**7,
            a/ρ**2*exp(-rc1/ρ),
            -42.0*c/rc2**8,
            0,0,0,0])
x = np.linalg.solve(H, F)
d=[x[0],x[1],x[2],x[3],x[4],x[5]]
b=[x[6],x[7],x[8],x[9]]

at1="O_s"
at2="O_s"
f=open("TABLE","w")
f.write("{0:72s}\n".format('table for O_s O_s interaction'))
f.write("{0:20.10f}{1:20.10f}{2:10d}\n".format(delpot,rcut,ngrid))
f.write("{0:8s}{1:8s}{2:20.10f}{3:20.10f}\n".format(at1,at2,elrc,vlrc))

UU=[ U(delpot*i) for i in range(1,ngrid+1,1) ]
GG=[ G(delpot*i) for i in range(1,ngrid+1,1) ]
for i in range((ngrid+3)//4):
  sx=4*i
  nx=min(4*i+4,ngrid)
  f.write("{0:17.12e} {1:17.12e} {2:17.12e} {3:17.12e}\n".format(*UU[sx:nx]))
for i in range((ngrid+3)//4):
  sx=4*i
  nx=min(4*i+4,ngrid)
  f.write("{0:17.12e} {1:17.12e} {2:17.12e} {3:17.12e}\n".format(*GG[sx:nx]))
f.close()
