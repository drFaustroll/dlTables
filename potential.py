#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

__author__ = "Alin Marin Elena <alin@elena.space>"
__copyright__ = "Copyright© 2017 Alin M Elena"
__license__ = "MIT"
__version__ = "1.0"

from math import *

#U(r)=a*exp(-r/ρ)-c/r**6
def U(r,a,ρ,c):
  return a*exp(-r/ρ)-c/r**6

#G(r)=-r*∂U/∂r
def G(r,a,ρ,c):
    return r*a/ρ*exp(-r/ρ)-6.0*c/r**6

#elrc=∫_rcut^∞U(r)r^2dr
def Elrc(rc,a,ρ,c):
    return a*ρ*(rc**2+2.0*ρ*rc+2.0*ρ**2)*exp(-rc/ρ)-c/(3.0*rc**3)

#vlrc=∫_rcut^∞ r*∂U/∂r*r^2dr
def Vlrc(rc,a,ρ,c):
    return -a*(rc**3+3.0*ρ*rc**2+6.0*ρ**2*rc+6.0*ρ**3)*exp(-rc/ρ)+2.0*c/rc**3

a=11272.6
ρ=0.1363
c=134.0

ngrid=1504
rcut=15.0
delpot=rcut/(ngrid-4)

elrc=Elrc(rcut,a,ρ,c)*9648.530821
vlrc=Vlrc(rcut,a,ρ,c)*9648.530821

at1="O_s"
at2="O_s"
f=open("TABLE","w")
f.write("{0:72s}\n".format('table for O_s O_s interaction'))
f.write("{0:20.10f}{1:20.10f}{2:10d}\n".format(delpot,rcut,ngrid))
f.write("{0:8s}{1:8s}{2:20.10f}{3:20.10f}\n".format(at1,at2,elrc,vlrc))

UU=[ U(delpot*i,a,ρ,c) for i in range(1,ngrid+1,1) ]
GG=[ G(delpot*i,a,ρ,c) for i in range(1,ngrid+1,1) ]
for i in range((ngrid+3)//4):
  sx=4*i
  nx=min(4*i+4,ngrid)
  f.write("{0:17.12e} {1:17.12e} {2:17.12e} {3:17.12e}\n".format(*UU[sx:nx]))
for i in range((ngrid+3)//4):
  sx=4*i
  nx=min(4*i+4,ngrid)
  f.write("{0:17.12e} {1:17.12e} {2:17.12e} {3:17.12e}\n".format(*GG[sx:nx]))
f.close()
