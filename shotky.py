#!/usr/bin/env python3

import random
import sys, getopt

def readConfig(filename,at,cell):
   with open(filename) as f:
       title=f.readline()
       lvl,imcon,n= (int(i) for i in f.readline().strip().split())
       for i in range(3):
           l=f.readline().strip().split()
           cell.append([float(l[0]),float(l[1]),float(l[2])])
       for i in range(n):
           nm=f.readline().strip().split()[0]
           r=f.readline().strip().split()
           at.append([nm,float(r[0]),float(r[1]),float(r[2])])
       return lvl,imcon,len(at)

def vacancies(at,v,i):
    n=len(at)-1
    k=0

    for j in sorted(random.sample(range(n//3),v),reverse=True):
        if (k<i):
           at[3*j]=['He',at[3*j][1],at[3*j][2],at[3*j][3]]
           at[3*j+1]=['He',at[3*j+1][1],at[3*j+1][2],at[3*j+1][3]]
           at[3*j+2]=['He',at[3*j+2][1],at[3*j+2][2],at[3*j+2][3]]
           at[n-3*k],at[3*j+2] = at[3*j+2],  at[n-3*k]
           at[n-3*k-1],at[3*j+1] = at[3*j+1],  at[n-3*k-1]
           at[n-3*k-2],at[3*j] = at[3*j],  at[n-3*k-2]
        else:
           del at[3*j+2]
           del at[3*j+1]
           del at[3*j]
        k=k+1   

def writeConfig(nconfig,ats,cell,lvl,imcon):
    n=len(ats)
    f=open(nconfig,"w")
    f.write("{0:72s}\n".format("PuO2 with defects"))
    f.write("{0:10d}{1:10d}{2:10d}{3:42s}\n".format(lvl,imcon,n,' '))
    f.write('{0:20.10f}{1:20.10f}{2:20.10f}{3:12s}\n'.format(*cell[0],' '))
    f.write('{0:20.10f}{1:20.10f}{2:20.10f}{3:12s}\n'.format(*cell[1],' '))
    f.write('{0:20.10f}{1:20.10f}{2:20.10f}{3:12s}\n'.format(*cell[2],' '))
    for i in range(n):
        f.write("{0:8s}{1:10d}{2:54s}\n{3:20.10f}{4:20.10f}{5:20.10f}{6:12s}\n".format(
            ats[i][0], i+1,' ', ats[i][1], ats[i][2], ats[i][3],' '))
    f.close()

def writeFIELD(field,n,i):
    f=open(field,"w")
    f.write("{0:72s}\n".format("PuO2 with He using Arima et al. and Grimes et al. potentials"))
    f.write("{0:72s}\n".format("units eV"))
    if (i>0):
       f.write("molecular types {0:d}\n".format(2))
    else:
       f.write("molecular types {0:d}\n".format(1))
    f.write("{0:72s}\n".format("PuO2"))
    f.write("nummols {0:d}\n".format((n-3*i)//3))
    f.write("{0:72s}\n".format("atoms 3"))
    f.write("{0:72s}\n".format("Pu          244.0000   2.70    1    0    1"))
    f.write("{0:72s}\n".format("O            16.0000   -1.35   2    0    1"))
    f.write("{0:72s}\n".format("finish"))
    if (i>0):
       f.write("{0:72s}\n".format("He"))
       f.write("nummols {0:d}\n".format(i*3))
       f.write("{0:72s}\n".format("atoms 1"))
       f.write("{0:72s}\n".format("He            4.0000   0.00   1   0   1"))
       f.write("{0:72s}\n".format("finish"))
    if (i>0):
       f.write("vdw {0:d}\n".format(5))
    else:
       f.write("vdw {0:d}\n".format(2))
    f.write("{0:72s}\n".format("Pu O buck 57424.5891 0.1985 0.0"))
    f.write("{0:72s}\n".format("O O buck 978.7081 0.332 17.3542"))
    if (i>0):
       f.write("{0:72s}\n".format("He He 12-6 69.3559 0.493712"))
       f.write("{0:72s}\n".format("He O 12-6 2247.836 11.762"))
       f.write("{0:72s}\n".format("He Pu 12-6 500.425 7.366"))
    f.write("{0:72s}\n".format("close"))
 

    f.close()

vac=0
he=0
try:
    opts, args = getopt.getopt(sys.argv[1:],"hv:i:",["vacancy=","impurity="])
except getopt.GetoptError:
   print('./shotky -v 10')
   print('or')
   print('./shotky -i 10')
   sys.exit(2)
for opt, arg in opts:
   if opt == '-h':
      print('./shotky -v 10')
      print('or')
      print('./shotky -i 10')
      sys.exit()
   elif opt in ("-v", "--vacancy"):
      vac = int(arg)
   elif opt in ("-i", "--impurity"):
      he = int(arg)

if (he!=0):
    print("Creating impurities for {0:d} sites\n".format(he))
if (vac!=0):
    print("Creating vacancies for {0:d} sites\n".format(he))

if (he==0 and vac==0):
    print("Nothing to do!!! Try to create a vacancy or impurity at least")
    sys.exit()

if (he>0 and vac==0):
    vac=he
if (he>vac):
    print("you cannot create more impurities than vacancies")
    sys.exit()

atoms=[]
cell=[]
lvl,imcon,n=readConfig("PuO2.CONFIG",atoms,cell)
print("Read {0:d} atoms from file".format(n))
random.seed(42)
vacancies(atoms,vac,he)
print("write {0:d} atoms to CONFIG".format(len(atoms)))
writeConfig("CONFIG",atoms,cell,lvl,imcon)
print("write  FIELD")
writeFIELD("FIELD",len(atoms),he)
