#!/usr/bin/env python
# -*- coding: UTF-8 -*-

__author__ = "Alin Marin Elena <alin@elena.space>"
__copyright__ = "Copyright© 2017 Alin M Elena"
__license__ = "MIT"
__version__ = "1.0"

data = []
nd=0
with open("STATIS",'r') as s:
    s.readline()
    s.readline()
    ln=s.readline()
    while ln:
      l=ln.split()
      step=int(l[0])
      time=float(l[1])
      nd=int(l[2])
      nl=nd//5 if nd % 5 ==0 else nd//5 + 1
      ls=[0.0]*(nd+2)
      ls[0]=step
      ls[1]=time
      for i in range(nl):
        l=s.readline().split()
        for k in range(len(l)):
          ls[i*5+k+2]=float(l[k])
      data.append(ls)
      ln=s.readline()
n=len(data)
for i in range(nd):
  fn="statis_col_{0:d}.dat".format(i+1)
  f=open(fn,'w')
  for k in range(n):
    f.write("{0:d} {1:f} {2:f}\n".format(data[k][0],data[k][1],data[k][i+2]))
  f.close()
